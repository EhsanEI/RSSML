'''
Created on Sep 19, 2016

@author: ehsan
'''


import numpy as np
import scipy
from scipy.stats import mode

def search(query_data, pool_data, metric='euclidean'):
    if metric not in ['euclidean', 'hamming']:
        print 'metric must be one of (euclidean, hamming)'
        return None
    
    nquery = query_data/ np.linalg.norm(query_data,axis=1)[:,np.newaxis]
    ndata = pool_data/ np.linalg.norm(pool_data,axis=1)[:,np.newaxis]    
    dist=scipy.spatial.distance.cdist(nquery,ndata,metric)
    sorted_idx=np.argsort(dist,axis=1)

    return sorted_idx

def getTarget(query_data, pool_data, pool_label, metric='euclidean'):
    sorted_ret=search(query_data,pool_data,metric=metric)
    sorted_result=sorted_ret.astype(int)
#     print sorted_result.shape
#     print sorted_result
    sorted_target=np.zeros(sorted_result.shape)
    for i in range(sorted_result.shape[0]):
            sorted_target[i,:] = pool_label[sorted_result[i,:],0]
    return sorted_target

def computeMap(query_data, pool_data, query_label, pool_label):
    target = getTarget(query_data, pool_data, pool_label)
    result = (target[:,1:] == query_label)
    cumsum=result.cumsum(axis=1)
    prec=cumsum / np.arange(1.0,1+result.shape[1])
    total_relevant=cumsum[:,-1]+1e-5
    ap=np.sum((prec*result),axis=1)/total_relevant
    return float("%.5f" % (np.mean(ap)))

def computeKnn(query_data, pool_data, query_label, pool_label, k=10):
    target = getTarget(query_data, pool_data, pool_label)
    result = mode(target[:,1:k+1],axis=1)[0] == query_label
    
    return float("%.5f" % (np.sum(result)/(1.0*result.shape[0])))
