'''
Created on Sep 18, 2016

@author: ehsan
'''

import theano
import theano.tensor as T
import numpy as np
import scipy
from sklearn.datasets import load_iris
from sklearn.svm import LinearSVC
from sklearn.cross_validation import train_test_split
from sklearn.neighbors.unsupervised import NearestNeighbors
from evaluation import computeKnn, computeMap
from sklearn.datasets.base import load_digits
from matplotlib import pyplot as plt
import scipy.io as sio
from os.path import join as oj
from sklearn.neighbors import KNeighborsClassifier as KNN

class AttributeDict(dict):
    __getattr__ = dict.__getitem__

    def __setattr__(self, a, b):
        self.__setitem__(a, b)

def load_uci(name):
    dataset = sio.loadmat(oj('dat','uciml.mat'))
    data = AttributeDict()
    data.data = dataset['da_'+name]
    data.target = dataset['la_'+name].flatten()
    return data

def load_mnist():
    dataset = sio.loadmat(oj('dat','mnist.mat'))
    data = AttributeDict()
    data.data = dataset['da_mnist']
    data.target = dataset['la_mnist'].flatten()
    return data

def plot_unsupervised(X, S, p):
    print np.hstack((X,p.reshape((p.shape[0],1))))
    plt.scatter(X[:,0], X[:,1], c=p)
    plt.show()

def psd(A, epsilon=0):
    n = A.shape[0]
    eigval, eigvec = T.nlinalg.eig(A)
    val = T.maximum(eigval,epsilon)[None,:]
    vec = eigvec
    TT = 1/((vec*vec).dot(val.T))
    TT = T.sqrt(T.diag(TT.reshape((n,))))
    B = TT.dot(vec).dot(T.diag(T.sqrt(val).reshape((n,))))
    out = B.dot(B.T)
    return out

def isPSD(A, tol=1e-8):
    E,_ = scipy.linalg.eigh(A)
    return np.all(E > -tol)

def RSSML(X_np, triplets, c=0.01, lr=0.0001, similarity='binary', k=10, n_epoch=20):
    in_dim = X_np.shape[1]
    train_cnt = X_np.shape[0]
    triplet_cnt = triplets.shape[0]
    
    # similarity
    print 'computing similarity'
    S = np.zeros((train_cnt,train_cnt))
    nbrs = NearestNeighbors(n_neighbors=k+1, n_jobs=8).fit(X_np)
    _, indices = nbrs.kneighbors(X_np)
    indices = indices[:,1:]
    if(similarity=='binary'):
        for i in xrange(S.shape[0]):
            S[i,indices[i,:]]=1
    else:
        raise Exception('not implemented')
    
    # density
    print 'computing density'
    p = np.zeros(train_cnt)
    h = 1.0
    coef = 1.0/(k*(h**in_dim))
    for ind in xrange(train_cnt):
        for ind2 in indices[ind,:]:
            dist = np.sum(((X_np[ind,...] - X_np[ind2,...])**2)/(2*(h**2))) 
            p[ind] += coef*np.exp(-dist)
    p /= np.max(p)
    
#     plot_unsupervised(X_np[:,:2], S, p)
    
    # W, D, and L
    print 'computing regularizer parameters'
    L = np.zeros((train_cnt, train_cnt))
    for ind in xrange(train_cnt):
        Wi = np.zeros((train_cnt, train_cnt))
        for ind2 in indices[ind,:]:
            Wi[ind, ind2] = p[ind]*S[ind,ind2]
            Wi[ind2, ind] = p[ind]*S[ind,ind2]
        Dwi = np.diag(np.sum(Wi,axis=1))
        L += Dwi - Wi
    XLXt_np = X_np.T.dot(L).dot(X_np)

    # tau
    print 'building triplets'
    tau1_np = np.zeros((triplet_cnt, in_dim))
    tau2_np = np.zeros((triplet_cnt, in_dim))
    tau3_np = np.zeros((triplet_cnt, in_dim))
    for ind in xrange(triplet_cnt):
        tau1_np[ind,...] = X_np[triplets[ind,0],...]
        tau2_np[ind,...] = X_np[triplets[ind,1],...]
        tau3_np[ind,...] = X_np[triplets[ind,2],...]
    
    
    ## GRADIENT DESCENT
                
    # inputs
    tau1 = T.matrix('tau1')
    tau2 = T.matrix('tau2')
    tau3 = T.matrix('tau3')
    XLXt = T.matrix('XLXt')
    
    # params
    A = theano.shared(value=np.eye(in_dim, dtype=theano.config.floatX), name='A', borrow=True)
    
    # regularizer
    reg = (XLXt.dot(A)).trace()
    
    # supervised cost
    diff_ij = tau1 - tau2
    Dij = T.diag(diff_ij.dot(A).dot(diff_ij.T))
    diff_ik = tau1 - tau3
    Dik = T.diag(diff_ik.dot(A).dot(diff_ik.T))
    violates = (Dik - Dij) < 1
    violate_cnt = violates.sum()
    triplet_cost = ((Dij - Dik)*violates).sum()
    cost = triplet_cost + violate_cnt
    
    # loss
    loss = c*reg + (1-c)*cost
    
    g_A = T.grad(cost=loss, wrt=A)
    
    updates = [(A, psd(A-lr*g_A))]
    
    train_model = theano.function(inputs=[tau1, tau2, tau3, XLXt], outputs=[A, loss, reg, violate_cnt, triplet_cost], updates=updates, on_unused_input='warn')
    
    print 'gradient descent'
    for e in xrange(n_epoch):
        A_np, loss_np, reg_np, violate_cnt_np, triplet_cost_np = train_model(tau1_np, tau2_np, tau3_np, XLXt_np)
        print '-------------'
        print 'epoch:', e
        print 'monitor:', loss_np, reg_np, violate_cnt_np, triplet_cost_np
        print 'A limits:', np.max(A_np), np.min(A_np)  
        assert(isPSD(A_np))
        if violate_cnt_np == 0:
            print 'no violation of constraints!'
            break 
    
    return A_np
    

if __name__ == '__main__':
#     data = load_uci('wine')
    data = load_digits()
    X_train, X_test, y_train, y_test = train_test_split(data.data, data.target, test_size=0.3, random_state=42)
    train_cnt = y_train.shape[0]
    test_cnt = y_test.shape[0]
    
    print X_train.shape, X_test.shape
    
    np.random.seed(42)
    
    t_cnt = 1000
    triplets = np.zeros((t_cnt,3))
    
    num_cats = np.max(y_train)+1
    sim_pool = {}
    dis_pool = {}
    for c in xrange(num_cats):
        sim_pool[c] = [ind for ind in xrange(train_cnt) if y_train[ind]==c]
        dis_pool[c] = np.array(list(set(np.arange(train_cnt))-set(sim_pool[c])))
    
    for t in xrange(t_cnt):
        anchor = np.random.randint(train_cnt)
        triplets[t,0] = anchor
        y = y_train[anchor]
        triplets[t,1] = np.random.choice(sim_pool[y])
        triplets[t,2] = np.random.choice(dis_pool[y])
        assert y_train[triplets[t,1]] == y
        assert y_train[triplets[t,2]] != y 
        
    
    A = RSSML(X_train, triplets)
    U = np.real(scipy.linalg.sqrtm(A))
    X_train_transform = X_train.dot(U)
    X_test_transform = X_test.dot(U)
    
    print '---'
    print 'MAP before:', computeMap(X_test, X_test, y_test[:,np.newaxis], y_test[:,np.newaxis])
    print 'KNN before:', computeKnn(X_test, X_train, y_test[:,np.newaxis], y_train[:,np.newaxis], k=1)
    print 'MAP after:', computeMap(X_test_transform, X_test_transform, y_test[:,np.newaxis], y_test[:,np.newaxis])
    print 'KNN after:', computeKnn(X_test_transform, X_train_transform, y_test[:,np.newaxis], y_train[:,np.newaxis], k=1)